import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Person> persons = List.of(new Person("Axel","man",200),
                new Person("Johan","man",3100.0),
                new Person("Ella","kvinna",5000.0),
                new Person("Vilma","kvinna",8700.0),
                new Person("Ida","kvinna",13800.0),
                new Person("Elin","kvinna",45700.0),
                new Person("Alexander","man",1200.0),
                new Person("Melker","man",1310.0),
                new Person("Mattias","man",400001.0),
                new Person("Anna","kvinna",28900.0));
        /*
    Med hjälp av streams, räkna ut:
    1.1 Snittlönen för kvinnorna respektive männen i listan
    1.2. Vem som har högst lön totalt
    1.3. Vem som har lägst lön totalt
     */
        double avgSalaryFemale = persons
                .stream()
                .filter(p -> p.getGender().equals("kvinna"))
                .mapToDouble(Person::getSalary)
                .summaryStatistics()
                .getAverage();

        double getAvgSalaryMale = persons
                .stream()
                .filter(p -> p.getGender().equals("man"))
                .mapToDouble(Person::getSalary)
                .summaryStatistics()
                .getAverage();

        System.out.println("Snittlön kvinna: " +avgSalaryFemale +"\nSnittlön man: " + getAvgSalaryMale);

        Person PersonWithHighestSalary = persons
                .stream()
                .sorted((p1, p2) -> (int) (p2.getSalary() - p1.getSalary()))
                .findFirst().get();
        System.out.println("Personen som har högst lön är " + PersonWithHighestSalary.getName());

        Person PersonWithLowestSalary = persons
                .stream()
                .sorted((p1, p2) -> (int) (p1.getSalary() - p2.getSalary()))
                .findFirst().get();
        System.out.println("Personen som har lägst lön är " + PersonWithLowestSalary.getName());
    }
}


class Person {
    private String name;
    private String gender;
    private double salary;

    public Person(String name, String gender, double salary) {
        this.name = name;
        this.gender = gender;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

}
