public abstract class Car {
    private String brand;
    private String model;
    private double topSpeed;

    public Car(String brand, String model, double topSpeed) {
        this.brand = brand;
        this.model = model;
        this.topSpeed = topSpeed;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public double getTopSpeed() {
        return topSpeed;
    }
}
