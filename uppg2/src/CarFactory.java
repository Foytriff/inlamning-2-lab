public class CarFactory {

    public Car makeNewCar(String carType) {
        if (carType.equals("BMW")){
            return new BMW();
        } else if (carType.equals("Volvo")){
            return new Volvo();
        } else if (carType.equals("Audi")){
            return new Audi();
        } else {
            System.out.println("Please select one of the given brands...");
            return null;
        }
    }
}
