import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        CarFactory factory = new CarFactory();

        Scanner scanner = new Scanner(System.in);
        System.out.println("What car should we make? (BMW/Volvo/Audi)");

        Car myCar = null;

        if(scanner.hasNextLine()){
            String carType = scanner.nextLine();
            myCar = factory.makeNewCar(carType);
            doCarStuff(myCar);
        }
    }

    public static void doCarStuff(Car myCar){
        if(myCar == null){
            return;
        }
        System.out.println("The car you created is of type: " + myCar.getBrand());
        System.out.println("The model of your car is: " + myCar.getModel() + ", and has a top speed of: " +myCar.getTopSpeed());
    }
}
