import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

class Counter {
    int count;

    public Counter() {
        this.count = 0;
    }

    public synchronized void increment(long amt){
        this.count += amt;
    }

    public int getCount(){
        return this.count;
    }
}

public class Main {
    public static void main(String[] args) throws InterruptedException {
        long start = 0;
        long end = 0;

        Counter counter = new Counter();

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                counter.increment(IntStream.range(1, 250000)
                        .filter(Main::findPrime)
                        .count());
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                counter.increment(IntStream.range(250000, 500001)
                        .filter(Main::findPrime)
                        .count());
            }
        });

        start = System.currentTimeMillis();
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        end = System.currentTimeMillis();
        System.out.println("Thread calc took: " + (end-start) + " and evaluated: " + counter.getCount());

        start = System.currentTimeMillis();
        long primeNumbers = IntStream.range(1, 500000).parallel()
                .filter(Main::findPrime)
                .count();
        end = System.currentTimeMillis();
        System.out.println("Parallel calc took: " + (end-start) + " and evaluated: " + primeNumbers);

    }

    public static boolean findPrime(int i){
        int maxTest = (int) Math.sqrt(Double.valueOf(i));
        return !IntStream.range(2, (maxTest + 1))
                .anyMatch(rangeNum -> i%rangeNum == 0);
    }
}