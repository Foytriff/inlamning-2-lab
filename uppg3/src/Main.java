import java.util.List;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        String regEx = ".*[aouiey].*[aouiey].*";
        Pattern pattern = Pattern.compile(regEx);
        List<String> myList = List.of("Hej", "det", "var", "en", "gång", "en", "fisk", "som", "drunknade", "fäst");
        myList.stream()
                .filter(pattern.asPredicate())
                .forEach(System.out::println);
    }
}